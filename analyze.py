import io
from functools import reduce
from Levenshtein import distance

from detect_delimiter import detect


# Get first {max_line_count} lines of csv file
# ({max_line_count} included headers)
def get_first_data_lines(file, max_line_count, encoding='utf-8'):
    try:
        buffer_size = 64 * 1024
        f = io.open(file, mode='r', encoding=encoding, buffering=buffer_size)
        line_counter = 0
        data = ''
        buffer = f.read(buffer_size)

        while len(buffer):
            data = data + buffer
            line_counter = line_counter + (len(data.split('\n')) - 1)
            if line_counter > max_line_count + 1:
                f.close()
                return '\n'.join(data.split('\n')[0:max_line_count])
            buffer = f.read(buffer_size)
        return ''.join(data)

    except Exception as e:
        raise e


# Get all lines from csv file
def get_all_data_lines(file, encoding='utf-8'):

    try:
        buffer_size = 64 * 1024
        f = io.open(file, mode='r', encoding=encoding, buffering=buffer_size)
        line_counter = 0
        data = ''
        buffer = f.read(buffer_size)

        while len(buffer):
            data = data + buffer
            line_counter = line_counter + (len(data.split('\n')) - 1)
            buffer = f.read(buffer_size)
        return ''.join(data)

    except Exception as e:
        raise e


# Detect delimiter in csv file
def detect_csv_delimiter(data, delimiters=None):
    if delimiters is None:
        delimiters = [',', ';', '\t']
    return detect(data, whitelist=delimiters)


# Remove prefix character from string if it exists
def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text


# Parse string to json
def string_to_json(string, delimiter):
    headers = [remove_prefix(s, ' ') for s in string.split('\n')[0].split(delimiter)]
    rows = [remove_prefix(s, ' ').split(delimiter) for s in string.split('\n')[1:]]

    json = []
    for i in range(len(rows)):
        obj = {}
        for j in range(len(headers)):
            obj[headers[j]] = rows[i][j]
        json.append(obj)
    return json


# Lambda for appending column data to column header
# Examples:
# 'OBJECT_STATUS': ['BEEINDIGD', 'Accident']
# 'REDEN_UIT': ['scrap', '']
def combine_rows_lambda(x, y):
    keys = [*y.keys()]
    for i in range(len(keys) - 1, -1, -1):
        if keys[i] not in x:
            x[keys[i]] = []
        x[keys[i]].append(y[keys[i]])
    return x


# Lambda for calculating distances
def map_lambda(x, y, obj, data):
    calculated = calculate_best_match(y, obj[y], data)
    x[calculated] = y
    return x


# Sorting array (not efficient, must be improved)
def bubble_sort_descending(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n - i - 1):
            if arr[j]['percentage'] < arr[j + 1]['percentage']:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]


# Calculate Levenshtein distance between proposed and real values, then calculating percentage of similarity
def calculate_distance(key, strarr, list):
    res = {
        'word': key,
        'results': []
    }
    strarr = [c.lower() for c in strarr]
    for j in range(len(strarr) - 1, -1, -1):
        for i in range(len(list) - 1, -1, -1):
            dist = distance(strarr[j], list[i].lower())
            res['results'].append({
                'str0': strarr[j],
                'str1': list[i],
                'distance': dist,
                'percentage': (100 - 100 * dist / (len(strarr[j]) + len(list[i]))),
            })

    bubble_sort_descending(res['results'])
    res['best_result'] = res['results'][0]['str1']
    return res


# Lambda for executing validation functions and calculating percentage of similarity
def data_matches_lambda(x, y, func, data):
    test = []
    ok = 0
    for v in data[y]:
        res = func(v)
        test.append(res)
        if res is True:
            ok = ok + 1
    x.append({'key': y, 'length': len(test), 'ok': ok, 'percentage': (ok / len(test)) * 100})
    return x


# Calculating row values match
def calculate_data_matches(key, func, data):
    res = {'results': reduce(lambda x, y: data_matches_lambda(x, y, func, data), [*data.keys()], [])}
    bubble_sort_descending(res['results'])
    res['best_result'] = res['results'][0]['key']
    return res


# Average and map Levenshtein distance percentages and data match percentages to column headers
def distance_map(distance_item, data_results):
    filter(lambda x: x['key'] == distance_item['str1'], data_results)
    distance_item['percentage'] = (distance_item['percentage'] * data_results[0]['percentage']) / 100
    return distance_item


# Calculate best result based on average of Levenshtein and data matches
def calculate_best_match(key, obj, data):
    result = {}
    if obj['dictionary'] is not None:
        result['distance'] = calculate_distance(key, obj['dictionary'], [*data.keys()])
        result['best_result'] = result['distance']['best_result']
    if obj['data']:
        result['data'] = calculate_data_matches(key, obj['data'], data)
        result['best_result'] = result['distance']['best_result']
    if obj['dictionary'] is not None and result['distance'] is not None:
        mapped = [distance_map(item, result['data']['results']) for item in result['distance']['results']]
        bubble_sort_descending(mapped)
        result['best_result'] = mapped[0]['str1']
    return result['best_result']


# Generate mapping for column data with probability score
def fetch_mapping(csv_data, domain):
    res = reduce((lambda x, y: combine_rows_lambda(x, y)), csv_data, {})
    res2 = reduce(lambda x, y: map_lambda(x, y, domain, res), [*domain.keys()], {})
    return res2


# Fetch data from csv with calculated mapping
def fetch_data(file_name, options, mapping):
    data_string = get_all_data_lines(file_name)
    data = string_to_json(data_string, options['delimiter'])
    for count, row in enumerate(data):
        for k, v in mapping.items():
            if k in row.keys():
                row[v] = row.pop(k)
    return data
