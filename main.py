import analyze
import validators


def do_suggestion(file_name, lines_to_analyze, domain):
    csv_data = analyze.get_first_data_lines(file_name, lines_to_analyze)
    delimiter = analyze.detect_csv_delimiter(csv_data, [',', ';', '\t'])

    print('Delimiter Found', delimiter)

    json = analyze.string_to_json(csv_data, delimiter)

    mapping = analyze.fetch_mapping(json, domain)
    print('Mapping suggestion: {}'.format(mapping))

    include = [key for key in mapping.keys()]
    options = {
        'include_columns': include,
        'delimiter': delimiter
    }

    print('Proposed Options: {}'.format(options))

    res = analyze.fetch_data(file_name, options, mapping)
    print(res)


if __name__ == '__main__':
    domain = {
        'vin': {
            'data': validators.is_vin,
            'dictionary': ['chassisnummer', 'vin']
        },
        'kenteken': {
            'dictionary': ['registrationnumber', 'kenteken'],
            'data': validators.is_license_plate
        },
        'dealer': {
            'dictionary': ['dealer'],
            'data': validators.is_dealer
        }
    }
    do_suggestion('test.csv', 3, domain)
