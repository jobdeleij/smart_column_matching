import re


def is_dealer(val):
    res = re.findall("\d{6}", val)
    return res if len(res) > 0 else None


def is_license_plate(val):
    stripped = val.upper().replace('-', '')
    matches = re.findall('\d+|\D+', stripped)
    if len(matches) <= 0:
        return False
    return len(val) <= 6 and 3 >= len(matches) > 1


def is_vin(val):
    # 17 chars
    res = re.findall("([A-Za-z0-9]){17}", val)
    return res if len(res) > 0 else None
